class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:index, :show]
  before_action :set_user, only: :show

  def index
    @users = User.where.not(id: current_user.id)
  end

  def show
    if params[:likes].present?
      @blogs = @user.blogs.where.not(user: @user)
    else
      @blogs = Blog.where(user: @user).all
    end
  end

  private
  def set_user
    @user = User.friendly.find(params[:id])
  end

end
